/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/birdFunction/bringMail.js":
/*!***********************************************!*\
  !*** ./application/birdFunction/bringMail.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst bringMail =() => {\r\n    console.log(\"bring mail\");\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (bringMail);\n\n//# sourceURL=webpack:///./application/birdFunction/bringMail.js?");

/***/ }),

/***/ "./application/birdFunction/carryEggs.js":
/*!***********************************************!*\
  !*** ./application/birdFunction/carryEggs.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst carryEggs = () => {\r\n    console.log(\"Carry eggs\");\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (carryEggs);\n\n//# sourceURL=webpack:///./application/birdFunction/carryEggs.js?");

/***/ }),

/***/ "./application/birdFunction/eat.js":
/*!*****************************************!*\
  !*** ./application/birdFunction/eat.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst eat = () => {\r\n    console.log(\"Eat\");\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (eat);\n\n//# sourceURL=webpack:///./application/birdFunction/eat.js?");

/***/ }),

/***/ "./application/birdFunction/fly.js":
/*!*****************************************!*\
  !*** ./application/birdFunction/fly.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst fly = () => {\r\n    console.log(\"Fly\");\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (fly);\n\n//# sourceURL=webpack:///./application/birdFunction/fly.js?");

/***/ }),

/***/ "./application/birdFunction/index.js":
/*!*******************************************!*\
  !*** ./application/birdFunction/index.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _fly__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./fly */ \"./application/birdFunction/fly.js\");\n/* harmony import */ var _swim__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./swim */ \"./application/birdFunction/swim.js\");\n/* harmony import */ var _carryEggs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./carryEggs */ \"./application/birdFunction/carryEggs.js\");\n/* harmony import */ var _eat__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./eat */ \"./application/birdFunction/eat.js\");\n/* harmony import */ var _bringMail__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./bringMail */ \"./application/birdFunction/bringMail.js\");\n\r\n\r\n\r\n\r\n\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\r\n    fly: _fly__WEBPACK_IMPORTED_MODULE_0__[\"default\"],\r\n    swim: _swim__WEBPACK_IMPORTED_MODULE_1__[\"default\"], \r\n    carryEggs: _carryEggs__WEBPACK_IMPORTED_MODULE_2__[\"default\"],\r\n    eat: _eat__WEBPACK_IMPORTED_MODULE_3__[\"default\"],\r\n    bringMail: _bringMail__WEBPACK_IMPORTED_MODULE_4__[\"default\"]\r\n});\r\n\r\n\n\n//# sourceURL=webpack:///./application/birdFunction/index.js?");

/***/ }),

/***/ "./application/birdFunction/swim.js":
/*!******************************************!*\
  !*** ./application/birdFunction/swim.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst swim = () => {\r\n    console.log(\"Swim\");\r\n\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (swim);\n\n//# sourceURL=webpack:///./application/birdFunction/swim.js?");

/***/ }),

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _birdFunction___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./birdFunction/ */ \"./application/birdFunction/index.js\");\n/* harmony import */ var _localStorage_ColorToLocalStorage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./localStorage/ColorToLocalStorage */ \"./application/localStorage/ColorToLocalStorage.js\");\n/* harmony import */ var _userStorage_userStorage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./userStorage/userStorage */ \"./application/userStorage/userStorage.js\");\n\r\n \r\n\r\n\r\ndocument.addEventListener('DOMContentLoaded', ()=>{\r\n    // class Work\r\n    \r\n    let b = new Ostrich();\r\n    b.eat();\r\n    b.carryEgs();\r\n\r\n    let dove = new Dove();\r\n    dove.eat();\r\n    dove.fly();\r\n    dove.carryEgs();\r\n    dove.bringMail();\r\n    \r\n    // Home work\r\n    //Task 1\r\n    document.body.style.backgroundColor = _localStorage_ColorToLocalStorage__WEBPACK_IMPORTED_MODULE_1__[\"default\"].getColorFromLocalStorage();\r\n    const changeColorButton = document.getElementById(\"changeColorButton\");\r\n    changeColorButton.addEventListener('click', ()=>{\r\n      _localStorage_ColorToLocalStorage__WEBPACK_IMPORTED_MODULE_1__[\"default\"].saveColorToLocalStorage(getRandomColor());\r\n      document.body.style.backgroundColor = _localStorage_ColorToLocalStorage__WEBPACK_IMPORTED_MODULE_1__[\"default\"].getColorFromLocalStorage();\r\n    });\r\n     \r\n    //Task 2\r\n    //localStorage.removeItem(\"users\");\r\n    let user = new _userStorage_userStorage__WEBPACK_IMPORTED_MODULE_2__[\"default\"].User(\"admin@example.com\", \"12345678\");\r\n    _userStorage_userStorage__WEBPACK_IMPORTED_MODULE_2__[\"default\"].saveUserToLocalStorage(user);\r\n    const root2 = document.getElementById(\"root2\");\r\n    root2.appendChild(_userStorage_userStorage__WEBPACK_IMPORTED_MODULE_2__[\"default\"].renderUserLoginForm());\r\n\r\n\r\n});\r\n\r\nfunction Ostrich() {\r\n    this.eat = _birdFunction___WEBPACK_IMPORTED_MODULE_0__[\"default\"].eat;\r\n    this.carryEgs = _birdFunction___WEBPACK_IMPORTED_MODULE_0__[\"default\"].carryEggs;\r\n}\r\n\r\nfunction Dove() {\r\n  this.eat = _birdFunction___WEBPACK_IMPORTED_MODULE_0__[\"default\"].eat;\r\n  this.carryEgs = _birdFunction___WEBPACK_IMPORTED_MODULE_0__[\"default\"].carryEggs;\r\n  this.fly = _birdFunction___WEBPACK_IMPORTED_MODULE_0__[\"default\"].fly;\r\n  this.bringMail = _birdFunction___WEBPACK_IMPORTED_MODULE_0__[\"default\"].bringMail;\r\n}\r\n\r\n\r\nfunction getRandomIntInclusive(min, max) {\r\n  min = Math.ceil(min);\r\n  max = Math.floor(max);\r\n  return Math.floor(Math.random() * (max - min + 1)) + min;\r\n}\r\n\r\nfunction getRandomColor(){\r\n  const r = getRandomIntInclusive(0, 255);\r\n  const g = getRandomIntInclusive(0, 255);\r\n  const b = getRandomIntInclusive(0, 255);\r\n  return ('rgb('+r+','+g+','+b+')');\r\n}\r\n\r\n\r\n /*\r\n\r\n  Задание:\r\n    Написать конструктор обьекта. Отдельные функции разбить на модули\r\n    и использовать внутри самого конструктора.\r\n    Каждую функцию выделить в отдельный модуль и собрать.\r\n\r\n    Тематика - птицы.\r\n    Птицы могут:\r\n      - Нестись\r\n      - Летать\r\n      - Плавать\r\n      - Кушать\r\n      - Охотиться\r\n      - Петь\r\n      - Переносить почту\r\n      - Бегать\r\n\r\n  Составить птиц (пару на выбор, не обязательно всех):\r\n      Страус\r\n      Голубь\r\n      Курица\r\n      Пингвин\r\n      Чайка\r\n      Ястреб\r\n      Сова\r\n\r\n */\r\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./application/localStorage/ColorToLocalStorage.js":
/*!*********************************************************!*\
  !*** ./application/localStorage/ColorToLocalStorage.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst saveColorToLocalStorage = (color) => {\r\n    if (color === undefined) {\r\n        localStorage.removeItem(\"color\");\r\n      } else {\r\n        localStorage.setItem(\"color\", color);\r\n      }\r\n}\r\n\r\nconst getColorFromLocalStorage = () => {\r\n    return localStorage.getItem('color');\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = ({saveColorToLocalStorage, getColorFromLocalStorage});\n\n//# sourceURL=webpack:///./application/localStorage/ColorToLocalStorage.js?");

/***/ }),

/***/ "./application/userStorage/userStorage.js":
/*!************************************************!*\
  !*** ./application/userStorage/userStorage.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nclass User {\r\n    constructor(login, password){\r\n        this.login = login;\r\n        this.password = password;\r\n    }\r\n    \r\n}\r\n\r\nconst saveUserToLocalStorage = (user) => {\r\n    let users = getUsersFromLocalStorage();\r\n    if(checkUserNotExists(user)){\r\n        users.push(user);\r\n    }\r\n    localStorage.removeItem(\"users\");\r\n    localStorage.setItem(\"users\", JSON.stringify(users));\r\n\r\n}\r\n\r\nconst checkUserNotExists = (user) => {\r\n    let users = getUsersFromLocalStorage();\r\n    let filterUsers=users.filter(f => f.login===user.login)\r\n    if(filterUsers.length===0)\r\n    {\r\n        return true;\r\n    }\r\n    return false;\r\n}\r\n\r\nconst userAllowedForLogin = (user) => {\r\n    let users = getUsersFromLocalStorage();\r\n    console.log(users);\r\n    let filterUsers=users.filter(f=> f.login===user.login && f.password === user.password);\r\n    if(filterUsers.length===0)\r\n    {\r\n        return false;\r\n    }\r\n    return true; \r\n}\r\n\r\nconst getUsersFromLocalStorage = () => {\r\n\r\n    let jsonUser = JSON.parse(localStorage.getItem('users'));\r\n    let users;\r\n    if(jsonUser === null)\r\n    {\r\n      users = new Array();\r\n      return users;\r\n    }\r\n    else{\r\n       users = jsonUser;\r\n    } \r\n    // if(!users.isArray)\r\n    // {\r\n    //     let usersList = new Array();\r\n    //     usersList.push(jsonUser);\r\n    //     console.log(usersList)\r\n    //     return usersList;\r\n    // }\r\n    return users;\r\n}\r\n\r\nconst getCurrentUsersFromLocalStorage = () => {\r\n    return JSON.parse(localStorage.getItem('currentUser'));  \r\n}\r\n\r\nconst saveCurrentUserToLocalStorage = (user) => {\r\n    localStorage.removeItem(\"currentUser\");\r\n    localStorage.setItem(\"currentUser\", JSON.stringify(user));\r\n}\r\n\r\nconst removeCurrentUserFromLocalStorage = (user) => {\r\n    localStorage.removeItem(\"currentUser\");\r\n}\r\n\r\nconst renderUserLoginForm = () => {\r\n    console.log('getCurrentUser', getCurrentUsersFromLocalStorage());\r\n    if(getCurrentUsersFromLocalStorage()===null){\r\n        console.log(\"renderForm\");\r\n        let formElement = document.createElement('form');\r\n        formElement.id = \"loginForm\"\r\n        formElement.innerHTML=`<label for=\"login\">Логін</label>\r\n        <input type=\"text\" name=\"login\" value=\"admin@example.com\" />\r\n        <label for=\"password\"> Пароль</label>\r\n        <input type=\"password\" name=\"password\" value=\"12345678\" />\r\n        <button type=\"button\" id=\"loginButton\">Отправить</button>`;\r\n\r\n        formElement.querySelector(\"#loginButton\").addEventListener('click', () => {\r\n       \r\n        const loginForm = document.getElementById(\"loginForm\");\r\n        const root2 = document.getElementById(\"root2\");\r\n        let userForLogin = new User(loginForm.login.value, loginForm.password.value);\r\n        console.log(\"userAllowedForLogin\", userAllowedForLogin(userForLogin));\r\n            if(userAllowedForLogin(userForLogin)){\r\n                root2.removeChild(root2.querySelector('form'));\r\n                saveCurrentUserToLocalStorage(userForLogin);\r\n                location.reload(); \r\n            }\r\n            else{\r\n                \r\n            }\r\n        });\r\n        return formElement;\r\n    }\r\n    else {\r\n        let userElement = document.createElement(\"div\");\r\n        let user = getCurrentUsersFromLocalStorage();\r\n        userElement.innerHTML=`<h3>Вітаю, ${user.login}!</h3> \r\n        <button type=\"button\" id=\"exitButton\">Вихід</button>`\r\n        let exitButton = userElement.querySelector(\"#exitButton\");\r\n        exitButton.addEventListener('click', ()=> {\r\n            removeCurrentUserFromLocalStorage();\r\n            location.reload(); \r\n        });\r\n        return userElement;\r\n    }\r\n\r\n   \r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = ({saveUserToLocalStorage, getUsersFromLocalStorage, User, \r\n    getCurrentUsersFromLocalStorage, saveCurrentUserToLocalStorage, checkUserNotExists, userAllowedForLogin, renderUserLoginForm});\n\n//# sourceURL=webpack:///./application/userStorage/userStorage.js?");

/***/ })

/******/ });