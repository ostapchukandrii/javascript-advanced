import fly from './fly';
import swim from './swim';
import carryEggs from './carryEggs';
import eat from './eat';
import bringMail from './bringMail'

export default {
    fly,
    swim, 
    carryEggs,
    eat,
    bringMail
};

