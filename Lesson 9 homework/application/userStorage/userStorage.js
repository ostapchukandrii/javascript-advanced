class User {
    constructor(login, password){
        this.login = login;
        this.password = password;
    }
    
}

const saveUserToLocalStorage = (user) => {
    let users = getUsersFromLocalStorage();
    if(checkUserNotExists(user)){
        users.push(user);
    }
    localStorage.removeItem("users");
    localStorage.setItem("users", JSON.stringify(users));

}

const checkUserNotExists = (user) => {
    let users = getUsersFromLocalStorage();
    let filterUsers=users.filter(f => f.login===user.login)
    if(filterUsers.length===0)
    {
        return true;
    }
    return false;
}

const userAllowedForLogin = (user) => {
    let users = getUsersFromLocalStorage();
    console.log(users);
    let filterUsers=users.filter(f=> f.login===user.login && f.password === user.password);
    if(filterUsers.length===0)
    {
        return false;
    }
    return true; 
}

const getUsersFromLocalStorage = () => {

    let jsonUser = JSON.parse(localStorage.getItem('users'));
    let users;
    if(jsonUser === null)
    {
      users = new Array();
      return users;
    }
    else{
       users = jsonUser;
    } 
    // if(!users.isArray)
    // {
    //     let usersList = new Array();
    //     usersList.push(jsonUser);
    //     console.log(usersList)
    //     return usersList;
    // }
    return users;
}

const getCurrentUsersFromLocalStorage = () => {
    return JSON.parse(localStorage.getItem('currentUser'));  
}

const saveCurrentUserToLocalStorage = (user) => {
    localStorage.removeItem("currentUser");
    localStorage.setItem("currentUser", JSON.stringify(user));
}

const removeCurrentUserFromLocalStorage = (user) => {
    localStorage.removeItem("currentUser");
}

const renderUserLoginForm = () => {
    console.log('getCurrentUser', getCurrentUsersFromLocalStorage());
    if(getCurrentUsersFromLocalStorage()===null){
        console.log("renderForm");
        let formElement = document.createElement('form');
        formElement.id = "loginForm"
        formElement.innerHTML=`<label for="login">Логін</label>
        <input type="text" name="login" value="admin@example.com" />
        <label for="password"> Пароль</label>
        <input type="password" name="password" value="12345678" />
        <button type="button" id="loginButton">Отправить</button>`;

        formElement.querySelector("#loginButton").addEventListener('click', () => {
       
        const loginForm = document.getElementById("loginForm");
        const root2 = document.getElementById("root2");
        let userForLogin = new User(loginForm.login.value, loginForm.password.value);
        console.log("userAllowedForLogin", userAllowedForLogin(userForLogin));
            if(userAllowedForLogin(userForLogin)){
                root2.removeChild(root2.querySelector('form'));
                saveCurrentUserToLocalStorage(userForLogin);
                location.reload(); 
            }
            else{
                
            }
        });
        return formElement;
    }
    else {
        let userElement = document.createElement("div");
        let user = getCurrentUsersFromLocalStorage();
        userElement.innerHTML=`<h3>Вітаю, ${user.login}!</h3> 
        <button type="button" id="exitButton">Вихід</button>`
        let exitButton = userElement.querySelector("#exitButton");
        exitButton.addEventListener('click', ()=> {
            removeCurrentUserFromLocalStorage();
            location.reload(); 
        });
        return userElement;
    }

   
}

export default {saveUserToLocalStorage, getUsersFromLocalStorage, User, 
    getCurrentUsersFromLocalStorage, saveCurrentUserToLocalStorage, checkUserNotExists, userAllowedForLogin, renderUserLoginForm};