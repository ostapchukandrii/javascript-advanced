const saveColorToLocalStorage = (color) => {
    if (color === undefined) {
        localStorage.removeItem("color");
      } else {
        localStorage.setItem("color", color);
      }
}

const getColorFromLocalStorage = () => {
    return localStorage.getItem('color');
}

export default {saveColorToLocalStorage, getColorFromLocalStorage};