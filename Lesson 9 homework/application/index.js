import birdFunction from './birdFunction/';
import colorLocalStorage from  './localStorage/ColorToLocalStorage'; 
import userLocalStorage from './userStorage/userStorage';

document.addEventListener('DOMContentLoaded', ()=>{
    // class Work
    
    let b = new Ostrich();
    b.eat();
    b.carryEgs();

    let dove = new Dove();
    dove.eat();
    dove.fly();
    dove.carryEgs();
    dove.bringMail();
    
    // Home work
    //Task 1
    document.body.style.backgroundColor = colorLocalStorage.getColorFromLocalStorage();
    const changeColorButton = document.getElementById("changeColorButton");
    changeColorButton.addEventListener('click', ()=>{
      colorLocalStorage.saveColorToLocalStorage(getRandomColor());
      document.body.style.backgroundColor = colorLocalStorage.getColorFromLocalStorage();
    });
     
    //Task 2
    //localStorage.removeItem("users");
    let user = new userLocalStorage.User("admin@example.com", "12345678");
    userLocalStorage.saveUserToLocalStorage(user);
    const root2 = document.getElementById("root2");
    root2.appendChild(userLocalStorage.renderUserLoginForm());


});

function Ostrich() {
    this.eat = birdFunction.eat;
    this.carryEgs = birdFunction.carryEggs;
}

function Dove() {
  this.eat = birdFunction.eat;
  this.carryEgs = birdFunction.carryEggs;
  this.fly = birdFunction.fly;
  this.bringMail = birdFunction.bringMail;
}


function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getRandomColor(){
  const r = getRandomIntInclusive(0, 255);
  const g = getRandomIntInclusive(0, 255);
  const b = getRandomIntInclusive(0, 255);
  return ('rgb('+r+','+g+','+b+')');
}


 /*

  Задание:
    Написать конструктор обьекта. Отдельные функции разбить на модули
    и использовать внутри самого конструктора.
    Каждую функцию выделить в отдельный модуль и собрать.

    Тематика - птицы.
    Птицы могут:
      - Нестись
      - Летать
      - Плавать
      - Кушать
      - Охотиться
      - Петь
      - Переносить почту
      - Бегать

  Составить птиц (пару на выбор, не обязательно всех):
      Страус
      Голубь
      Курица
      Пингвин
      Чайка
      Ястреб
      Сова

 */
