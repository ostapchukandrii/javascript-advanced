/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.


    Например:
      encryptCesar( 3, 'Word');
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1(3, 'Sdwq');
      decryptCesar1(...)
      ...
      decryptCesar5(...)

      "а".charCodeAt(); // 1072
      String.fromCharCode(189, 43, 190, 61) // ½+¾

*/
function encryptCesar(num, text)
{
  var res='';
  text.split("").forEach(element => {
    res+= String.fromCharCode(element.charCodeAt()+num);
  });
  console.log(res);
  return res;
}

function decryptCesar(num, text)
{
  var res='';
  text.split("").forEach(element => {
    res+= String.fromCharCode(element.charCodeAt()-num);
  });
  console.log(res);
  return res;
}

let encryptCesar1 = encryptCesar.bind(null, 1, "Word");
let encryptCesar2 = encryptCesar.bind(null, 2, "Word");
let encryptCesar3 = encryptCesar.bind(null, 3, "Word");
let encryptCesar4 = encryptCesar.bind(null, 4, "Word");
let encryptCesar5 = encryptCesar.bind(null, 5, "Word");
let decryptCesar1 = decryptCesar.bind(null, 1, encryptCesar1());
let decryptCesar2 = decryptCesar.bind(null, 2, encryptCesar2());
let decryptCesar3 = decryptCesar.bind(null, 3, encryptCesar3());
let decryptCesar4 = decryptCesar.bind(null, 4, encryptCesar4());
let decryptCesar5 = decryptCesar.bind(null, 5, encryptCesar5());
decryptCesar1();
decryptCesar2();
decryptCesar3();

//console.log(decryptCesar(3, encryptCesar( 3, 'Word')));