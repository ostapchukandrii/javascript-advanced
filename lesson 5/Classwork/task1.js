/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/
let Train = {
    name: "Thomas",
    speed: 300,
    passengerCount: 20,
    go: function() {
        return `Поезд ${this.name} везет ${this.passengerCount} со скоростью ${this.speed}`;
    },
    stop: function() { 
        this.speed = 0;
        return `Поезд ${this.name} остановился. Скорость ${this.speed}`
    },
    pickUpPassengers: function(x) {
        return this.passengerCount *= x
    }

  };
  console.log(Train);
  console.log(Train.go());
  console.log(Train.stop());
  console.log(Train.pickUpPassengers(2));
