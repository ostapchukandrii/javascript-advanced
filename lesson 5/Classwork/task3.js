/*

    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода
    2. Обьект должен иметь пару свойств (Имя, порода, status)
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака есть)
    4. Функция которая перебором выводит все свойства


    Dog {
      name: '',
      breed: '',
      status: 'idle',

      changeStatus: function(){...},
      showProps: function(){...}
    }

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }

  
    
*/

function Dog(name, breed, status) {
  this.name = name;
  this.breed = breed;
  this.status = status;

  this.changeStatus = function(statusValue){
    this.status = statusValue;
  }

  this.showProps = function(){
    for (var key in this) {
      console.log(`Ключ: ${key} значение: ${this[key]}`);
    }
  }
}

var Dog2 = {
  name: '',
  breed: '',
  status: 'idle',

  changeStatus: function(statusValue){this.status = statusValue;},
  showProps: function(){ 
    for (var key in this) {
      console.log(`Ключ: ${key} значение: ${this[key]}`);
    }
  }
}



let dog = new Dog("Tuzik", "husky", "Собака бежит");
dog.showProps();
dog.changeStatus("Собака есть");
dog.showProps();

var dog2 = Object.create(Dog2);
dog2.name = "Tuzik";
dog2.breed = "husky";
dog2.status = "Собака бежит";
dog2.showProps();
dog2.changeStatus("Собака есть");
dog2.showProps();