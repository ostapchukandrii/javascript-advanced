/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>


*/

function Comment(name, text) {
  this.name = name;
  this.text = text;
  this.likes = 0;
  this.avatarUrl;
  this.getAvatarUrl = () => {console.log('test',this.avatarUrl);}
};

Comment.prototype.avatarUrl="https://i2.wp.com/virtualproductions.co.za/wp-content/uploads/2018/04/user_image.jpg";
Comment.prototype.IncreaseNumberOfLikes = function(){
    this.likes++;
};

 let myComment1 = new Comment("Barack","Хороше розташування - в парку Муромець, гарна закрита територія, наявність парковки. Є все необхідне для проживання: водичка, тапочки, набори косметичні і для шиття, шампунь, гель. Непоганий продуманий інтер'єр в кантрі стилі Альтернатива готелю Борисфен.");
 myComment1.avatarUrl="https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/President_Barack_Obama.jpg/250px-President_Barack_Obama.jpg";
 let myComment2 = new Comment("George","Дуже привітний персонал, надзвичайне місце розташування та краєвиди,чистота готелю і невисокі ціни.","");
 myComment2.avatarUrl="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/George-W-Bush.jpeg/250px-George-W-Bush.jpeg"; 
 let myComment3 = new Comment("Max","Чудовий вигляд і дуже хороша кухня");
 let CommentsArray = [myComment1, myComment2, myComment3];

 function AddComent(comment)
 {
  let comments=document.getElementById("CommentsFeed");
  let commentDiv = document.createElement("div");
  commentDiv.classList.add("commentDiv");
  
  //Блок з картинками
  let imgDiv = document.createElement("div");
  imgDiv.classList.add("imgDiv");
  let img = document.createElement("img");
  img.classList.add("img-logo");
  img.src = comment.avatarUrl;
  imgDiv.appendChild(img);

  //Блок з іменем та тексом відгуку
  let pDiv = document.createElement("div");
  pDiv.classList.add("pDiv");
  let pName = document.createElement("p");
  pName.classList.add("pName");
  let text = document.createElement("p");
  text.classList.add("pText");
  pName.innerHTML = comment.name;
  text.innerHTML = comment.text;
  
  //Блок з рейтингом та кнопкою
  let pLikes =  document.createElement("p");
  pLikes.classList.add("pLikes");
  pLikes.innerText =  `Рейтинг: ${comment.likes}`;
  let button = document.createElement("button");
  button.type = "btn";
  button.innerText = "Вподобайка";
  
  //Додаємо картинку, ім'я, текст, рейтинг та кнопку в контейнер
  pDiv.appendChild(pName);
  pDiv.appendChild(text);
  pDiv.appendChild(pLikes);
  pDiv.appendChild(button);
  commentDiv.appendChild(imgDiv);
  commentDiv.appendChild(pDiv);

  //Підписуємося на натиск по кнопці
  button.addEventListener('click', clickAction(comment, commentDiv));

  //Виводимо результат
  comments.appendChild(commentDiv);
  
 }

 // Обробка натиску на кнопку
 let clickAction = (comment,commentDiv) => (e) => {
    comment.IncreaseNumberOfLikes();
    commentDiv.querySelector('.pLikes').innerText = `Рейтинг: ${comment.likes}`;
};

 CommentsArray.forEach(element => {AddComent(element);});
