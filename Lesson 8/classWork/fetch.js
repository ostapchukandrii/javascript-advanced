/*
  Задача:

  1. При помощи fetch получить данные:
     http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2

  2. Полученый ответ преобразовать в json вызвав метод .json с объекта ответа
  3. Выбрать случайного человека и передать в следующий чейн промиса
  4. Сделать дополнительный запрос на получение списка друзей человека
     http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2
     И дальше передать обьект:
      {
        name: userName,
        ...
        friends: friendsData
      }

     Подсказка нужно вызвать дополнительный fecth из текущего чейна.
     Для того что бы передать результат выполнения доп. запроса
     в наш первый промис используйте констуркцию:

      .then(
        response1 => {
          return fetch(...)
            .then(
              response2 => {
                ...
                формируете обьект в котором будут данные человека с
                первого запроса, например его name response1.name
                и друзья которые пришли из доп. запроса
              }
            )
        }
      )

  5. Вывести результат на страничку

  + Бонус. Для конвертации обьекта response в json использовать одну
    функцию.

*/


  // var a = fetch("http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2")
  // .then(a=> {
  //   console.log('test');
  //   console.log(a.json());
  //   //console.log(res);
  // });
  class BaseModel
  {
    constructor() {
      console.log('Base model', this);
    }

    AppendRenderInContainer(Id)
    {
      let a = this.render();
      document.getElementById(Id).innerHTML += a;
    }
  }
  class User extends BaseModel
  {
    constructor(Id, name, age, friends) {
      super();
      this.id = Id;
      this.name = name;
      this.age = age;
      this.friends = friends;
      this.render = this.render.bind(this);
    }

    render() {
      console.log('render',this);
      return `
      <div>
        <p>Ідентифікатор: ${this.id}</p>
        <p>Ім'я: ${this.name}</p>
        <p>Вік: ${this.age}</p>
        <span>Друзі:</span>
        ${this.renderFriends()}
      </div>`
    }

    renderFriends()
    {
      let text='<ul>'
      this.friends.forEach(element => {
        text +=`<li>${element.name}</li>`
      });
      text+='</ul>'
      return text;
    }
  }

  fetch("http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2")
  .then(res => res.json())
  .then(users => users[getRandomIntInclusive(0,users.length-1)])
  .then(user => convertToUser(user))
  .then(user => { 
     var myRequest = new Request("http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2")
     fetch(myRequest)
     .then(res => res.json())
     .then(users => {
       return users.filter( f=> f._id===user.id)[0];
     }) 
     .then(user => convertToUser(user))
    .then(user=>user.AppendRenderInContainer('renderContainer'))
    .catch(error => console.log(`Error: ${error}`));
    }
  )
  .catch(error => console.log(`Error: ${error}`));
  
  function convertToUser(user){
    return new User(user._id, user.name, user.age, user.friends);
  }
  
  function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
  // var Tvalue = JSON.stringify('{"_id": "59fd23f8d706997454ded42a", "about": "Nulla aute esse esse" }', function (key, value) {
  //   console.log("TTTTTest");
  //   return typeof value === 'number'
  //     ? value * 2 // Повернути подвоєне значення, якщо це число.
  //     : value     // Решту значень повернути без змін.
  // });
  // console.log(Tvalue);
 
    
  //   // res.forEach(element => {
  //   //   // console.log('test');
  //   //   // // var a = JSON.parse(element);
  //   //   console.log(element.id);
  //   //   console.log(element.name);
  //   // });
  // })
  // .catch();
  // .then(testFunc)
    // .then(test2Func)
    // .then( res => {
    //    return fetch()
    //     .then( friendsResponse)
    //     .then()
    // })
    // .then( func)
