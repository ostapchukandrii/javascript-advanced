/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    # | Company  | Balance | Показать дату регистрации | Показать адресс |
    1.| CompName | 2000$   | button                    | button
    2.| CompName | 2000$   | 20/10/2019                | button
    3.| CompName | 2000$   | button                    | button
    4.| CompName | 2000$   | button                    | button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/
  let companyTable='';
  class Company 
  {
    constructor(name, balance, address, registered) {
      this.name = name;
      this.balance = balance;
      this.registered = registered;
      this.address = `${address.house} ${address.street} Street, ${address.city}, ${address.state}, ${address.country}`
      this.render = this.render.bind(this);
    }
    renderStart() {
      return "<table border='1'><tr><th>Company</th><th>Balance</th><th>Показать дату регистрации</th><th>Показать адресс</th></tr>"
    }
    render() {
      return `<tr>
              <td>${this.name}</td>
              <td>${this.balance}</td>
              <td><button type="btn" class="showButtonRegistered">Показать</button><span class="showOnClick">${this.registered}</span></td>
              <td><button type="btn" class="showButtonAddress">Показать</button><span class="showOnClick">${this.address}</span></td>
              </tr>`;
    }
    
  }
   
  async function getCompanyList(){  
    let getUserResponse = await fetch("http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2")
    let companList = await getUserResponse.json();
    let myCompanyList=companList.map(company => new Company(company.company, company.balance, company.address, company.registered))    
    let render=document.getElementById('renderContainer');
    myCompanyList.forEach(element => {
      if(companyTable==='') companyTable = element.renderStart();
      companyTable+=element.render();
    });   
    companyTable += "</table>";
    render.innerHTML = companyTable;
    ShowSpanOnButtonClick("showButtonRegistered");
    ShowSpanOnButtonClick("showButtonAddress");
  }

    function ShowSpanOnButtonClick(className)
    {
      let buttons = document.getElementsByClassName(className);
      for (i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener("click", function() {
          this.style.display = 'none';
          let spans = this.parentNode.getElementsByClassName("showOnClick");
          Array.prototype.forEach.call(spans, function(el) {
            el.style.display = 'block';
          });
        });
      }
    }
  

  var a = getCompanyList();

 
