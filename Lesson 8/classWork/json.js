
/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Arary

  <form>
    <input name="name" />
    <input name="age"/>
    <input name="password"/>
    <button></button>
  </form>

  <input />

  -> '{"name" : !"23123", "age": 15, "password": "*****" }'


*/
document.addEventListener('DOMContentLoaded',()=>{
  const ObjectResult = document.getElementById("ObjectResult");
  const JsonResult = document.getElementById("JsonResult");
  ObjectResult.addEventListener('click', ()=> {
    let jsonString = JsonResult.innerHTML;
    console.log(JSON.parse(jsonString));
  })

  class FormConstructor{
    constructor(formId) {
      const myForm =document.getElementById(formId);
      this.form = myForm;
      this.state = {};
      this.changeData = this.changeData.bind(this);
      this.getJSON = this.getJSON.bind(this);
      this.renderJson = this.renderJson.bind(this);
      this.initHandlers();
    }

    initHandlers(){
      for (let i = 0, element; element = this.form.elements[i++];) {
        if( element.type !== 'submit' && element.name !== ""){
          this.state[element.name]=element.value;
          element.addEventListener('input', this.changeData);
        }
      }
    }

    getJSON(){
      return JSON.stringify( this.state );
    }

    renderJson(){
        const target = document.getElementById('JsonResult');
        target.innerText = this.getJSON();
    }

    changeData(e){
      let name = e.target.name;
      let value = e.target.value;
      if( value !== ""){
        this.state[name] = value;
      }
      this.renderJson();
    }
  }


  new FormConstructor('myForm');
  
});