/*

  Задание написать простой слайдер.

    Есть массив с картинками из которых должен состоять наш слайдер.
    По умолчанию выводим первый элмемнт нашего слайдера в блок с id='slider'
    ( window.onload = func(){...} // window.addEventListener('load', function(){...}) )
    По клику на PrevSlide/NextSlide показываем предыдущий или следующий сладй соответствено.

    Для этого необходимо написать 4 функции которые будут:

    1. Срабатывать на событие load обьекта window и загружать наш слайд по умолчанию.
    2. RenderImage -> Очищать текущий контент блока #slider. Создавать нашу картинку и через метод аппенд чайлд вставлять её в слайдер.
    3. NextSlide -> По клику на NextSilde передавать currentPosition текущего слайда + 1 в функцию рендера
    4. PrevSlide -> Тоже самое что предыдущий вариант, но на кнопку PrevSlide и передавать currentPosition - 1
      + бонус сделать так что бы при достижении последнего и первого слада вас после кидало на первый и последний слайд соответственно.
      + бонсу анимация появления слайдов через навешивание дополнительных стилей

*/

  var OurSliderImages = ['images/cat1.jpg', 'images/cat2.jpg', 'images/cat3.jpg', 'images/cat4.jpg', 'images/cat5.jpg', 'images/cat6.jpg', 'images/cat7.jpg', 'images/cat8.jpg'];
  var currentPosition = 0;

  document.addEventListener("DOMContentLoaded", function () {
    RenderImage();
  });

  document.getElementById('PrevSilde').addEventListener("click",PrevSlide);
  document.getElementById('NextSilde').addEventListener("click",NextSlide);

  function RenderImage() {
    const slider = document.getElementById("slider");
    let sliderImg=document.getElementById('sliderImg');
    if(sliderImg===null){
      sliderImg=document.createElement('img');
      sliderImg.src=OurSliderImages[currentPosition];
      sliderImg.id="sliderImg";
      sliderImg.classList.add("loadingImage");
      sliderImg.classList.add("loadedImage");
      slider.appendChild(sliderImg);
      return;
    }
    sliderImg.classList.remove("loadedImage");
    setTimeout(animation, 200);
    sliderImg.src=OurSliderImages[currentPosition];
  }

  function animation() {
    const sliderImg=document.getElementById('sliderImg');
    sliderImg.classList.add("loadedImage");
  }
  

  function NextSlide(){
    currentPosition++;
    currentPosition = currentPosition===OurSliderImages.length ? 0 : currentPosition;
    RenderImage();
  }
  function PrevSlide(){
    currentPosition--;
    currentPosition = currentPosition==-1 ? OurSliderImages.length-1 : currentPosition;
    RenderImage();
  }