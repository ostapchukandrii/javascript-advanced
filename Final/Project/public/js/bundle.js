/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./application/style.scss
var style = __webpack_require__(1);

// CONCATENATED MODULE: ./application/user/userModel.js
class UserModel {
    constructor(name, imgSrc, password, email, id=0){
        this.id = id;
        this.name = name;
        this.imgSrc = imgSrc;
        this.email = email;
        this.password = password;
    }

    render() {
        let userBlock = document.createElement("div");
        userBlock.classList.add("userBlock");
        userBlock.innerHTML = `<div><img src=${this.imgSrc} alt="userLogo"></div>
        <div class="userInfo"><h4>${this.name}</h4><p>Email: ${this.email}</p></div>`;
        return userBlock;
    }
}
// CONCATENATED MODULE: ./application/user/userRepository.js


class userRepository_UserRepository {
    constructor(){
        this.saveUser(new UserModel("Andrii", "https://i.pinimg.com/236x/58/83/46/5883460be4596cb67bd791acaeb32888.jpg", "321321", "ostapchukandrii@gmail.com", this.getUserId()));
        this.saveUser(new UserModel("Nazar", "https://pp.userapi.com/QKxHCl9M9_pCI8sEweYOhYzw5DlRGmWFs3cT9w/2J4Pih7JFZA.jpg", "321321", "nazarmyua@gmail.com",this.getUserId()));
    }

    saveUser(user){
        if(user.id===0){
            user.id=this.getUserId();
        }
        if(this.userNotExists(user))
        {
            let users = this.getUsers();
            users.push(user);
            localStorage.setItem("users",JSON.stringify(users));
        }
        else
        {
            return {error: "Помилка! Такий користувач уже існує."};
        }
        return user;
    }

    userNotExists(user){
        let users = this.getUsers();
        let filterUsers=users.filter((u)=>u.email===user.email);
        return filterUsers.length>0? false : true;
    }

    getUsers(){
        let users = JSON.parse(localStorage.getItem("users"));
        if(users === null)
        {
            users = new Array();
        }
        return users;
    }

    getUserById(id) {
        let users = this.getUsers();
        let filterUsers=users.filter((u)=>u.id===id);
        return filterUsers[0];
    }

    getUserId(){
       return this.getUsers().length;
    }

    login(email,password){
        let users = this.getUsers();
        let filterUsers=users.filter((u)=>u.email===email && u.password===password);
        if(filterUsers.length===0){
            return {error: "Помилка! Неправильний логін або пароль."};
        }
        let currentUser = filterUsers[0];
        this.setCurrentUser(currentUser);
        return currentUser;
    }

    // currentUser
    getCurrentUser (){
        let u = JSON.parse(localStorage.getItem("user"));
        if(u===null) 
        return u;
        return new UserModel(u.name, u.imgSrc, u.password, u.email, u.id);
    }

    removeCurrentUser(){
        localStorage.removeItem("user");
    }

    setCurrentUser(user){
        this.removeCurrentUser();
        localStorage.setItem("user",JSON.stringify(user));
    }

}
// CONCATENATED MODULE: ./application/posts/basePost/postBase.js


class postBase_PostBase {

    constructor({title, userId, rate, postId}) {
        this.postId = postId;
        this.title = title;
        this.userId = userId;
        this.rate = rate;        
    }

    render(addtionalClass, content) {

        let userRepository = new userRepository_UserRepository();
        let user = userRepository.getUserById(this.userId);
 
        let authorImg = user.imgSrc;
        let authorName = user.name;
        let publishDate = "4h";
        
        let baseElement = document.createElement("div");
        baseElement.classList.add("post");

        if(addtionalClass){
            baseElement.classList.add(addtionalClass);
        }
        
        //add header
        let postHeader = document.createElement("div");
        postHeader.classList.add("post-header");        
        postHeader.innerHTML = `<img class="avatar" src="${authorImg}" alt="Author">
                                <p class="author">${authorName}</p> <p>|</p> <p class="publishDate">${publishDate}</p>`;
        baseElement.appendChild(postHeader);
        
        //add title
        let postTitle = document.createElement("div");
        postTitle.classList.add("post-title");
        postTitle.innerText = this.title;
        baseElement.appendChild(postTitle);

        //add content
        let postContent = document.createElement("div");
        postContent.classList.add("post-content");
        if(content){
            postContent.innerHTML = content.innerHTML;
        } else {
            postContent.innerText = "CONTENT";
        }
        baseElement.appendChild(postContent);

        //add post info
        let rate = document.createElement("div");
        rate.classList.add("post-info");
        rate.innerHTML = `<button class="up">UP</button>
                          <p class="rate">${this.rate.length}</p>
                          <button class="down">DOWN</button>
                          `
        baseElement.appendChild(rate);

        baseElement.querySelector(".up").addEventListener("click", upRate(baseElement));
        baseElement.querySelector(".down").addEventListener("click", downRate(baseElement));
        
        return baseElement; 
    }
} 

const upRate = (post) => {
    return function(e){
        post.querySelector(".rate").innerText = +post.querySelector(".rate").innerText + 1;
    }
}

const downRate = (post) => {
    return function(e){
        post.querySelector(".rate").innerText = +post.querySelector(".rate").innerText - 1;
    }
}

// CONCATENATED MODULE: ./application/posts/imagePost/postImage.js


class postImage_PostImage extends postBase_PostBase{
    constructor({title, userId, rate, imgSrc, postId=0}){
        super({title, userId, rate, postId});
        this.imgSrc = imgSrc;
    }

    render(){
        let content = document.createElement("div");
        content.innerHTML = `<img class="avatar" src="${this.imgSrc}" alt="Author">`;
        return super.render("post-img",content);
    }
}
// CONCATENATED MODULE: ./application/posts/textPost/postText.js


class postText_PostText extends postBase_PostBase{
    constructor({title, userId, rate, postText, postId=0}){
        super({title, userId, rate, postId});
        this.postText = postText;
    }

    render(){
        let content = document.createElement("div");
        content.innerHTML = this.postText;
        return super.render("post-text",content);
    }
}
// CONCATENATED MODULE: ./application/posts/videoPost/postVideo.js


class postVideo_PostVideo extends postBase_PostBase{
    constructor({title, userId, rate, videoSrc, postId=0}){
        super({title, userId, rate, postId});
        this.videoSrc = videoSrc;
    }

    render(){
        let content = document.createElement("div");
        content.innerHTML = `<iframe width="650" height="488"
                                src="${this.videoSrc}">
                            </iframe>`;
        return super.render("post-video",content);
    }
}
// CONCATENATED MODULE: ./application/posts/postRepository/postRepository.js




class postRepository_PostRepository {
    constructor(){
          // Posts

          let postImage = new postImage_PostImage({postId:1, title: "Title test", userId: 1, rate: [1], imgSrc: "https://img-9gag-fun.9cache.com/photo/aA3K830_460swp.webp"});
          let postImage2 = new postImage_PostImage({postId:2, title: "Title test", userId: 0, rate: [0], imgSrc: "https://img-9gag-fun.9cache.com/photo/aA3K830_460swp.webp"});

          let postTextModel = {postId: 3, title: "Title test", userId: 1, rate: [1],
          postText: `<p>fjdhs jafhsdk  hfaskjdhkj sadhfk hsakjhfj kshfjkh fjsadf sjafhjash</p>
          <p>fjdhs jsdk  hfaskjdhkj sadhfk hsakjhfj afhsdk  hfaskjdhkj sadhfk hsakjhfj kshfsdk  hfaskjdhkj sadhfk hsakjhfj jkh fjsadf sjafhjash</p>
          <p>fjdhs jasdk  hfaskjdhkj sadhfk hsakjhfj fhsdk  hfaskjdhkj sadhfk hsakjhfj kshfjkh fjsadf sjafhjash</p>
          <p>fjdhs jafhsdk  hfaskjdhkj sadhfk hsakjhfsdk  hfaskjdhkj sadhfk hsakjhfj sdk  hfaskjdhkj sadhfk hsakjhfj sdk  hfaskjdhkj sadhfk hsakjhfj j kshfjkh fjsadf sjafhjash</p>
          <p>fjdhs jafhsdk  hfaskjdhkj sadhfk hsakjhfj kssdk  hfaskjdhkj sadhfk hsakjhfj sdk  hfaskjdhkj sadhfk hsakjhfj hfjkh fjsadf sjafhjash</p>
          <p>fjdhs jafhsdk  hfaskjdhkj sadhfk hsakjhfj kshfjkh fjsadf sjafhjash</p>
          <p>fjdhs jafhsdk  hfaskjdhkj sadhfk hsakjhsdk  hfaskjdhkj sadhfk hsakjhfj sdk  hfaskjdhkj sadhfk hsakjhfj sdk  hfaskjdhkj sadhfk hsakjhfj fsdk  hfaskjdhkj sadhfk hsakjhfj sdk  hfaskjdhkj sadhfk hsakjhfj vj kshfjkh fjsadf sjafhjash</p>
          <p>fjdhs jafhsdk  hfaskjdhkj sadhfk hsakjhfj sdk  hfaskjdhkj sadhfk sdk  hfaskjdhkj sadhfk hsakjhfj vhsasdk  hfaskjdhkj sadhfk hsakjhfj sdk  hfaskjdhkj sadhfk hsakjhfj vkjhfj kshfjkh fjsadf sjafhjash</p>
          `};

           let postText = new postText_PostText(postTextModel);
           let postVideo = new postVideo_PostVideo({postId: 4, title: "Title test",userId: 1, rate: [1], videoSrc: "https://www.youtube.com/embed/tgbNymZ7vqY"});
           
           this.save(postText);
           this.save(postImage);
           this.save(postVideo);
           this.save(postImage2);
    }

    save(post){
        console.log("postForSave",post);
        if(post.postId===0 || post.postId===undefined){
            post.postId=this.getPostId();
        }


        if(this.postNotExists(post))
        {
            let posts = this.getPosts();
            posts.push(post);
            localStorage.setItem("posts",JSON.stringify(posts));
        }
        else
        {
            return {error: "Помилка! Пост з таким ідентифікатором уже існує."};
        }
        console.log("newPost",post);
        return post;
    }

    getPosts(){
        let posts = JSON.parse(localStorage.getItem("posts"));
        if(posts === null)
        {
            posts = new Array();
        }
        return posts;
    }

    getPostId(){
        return this.getPosts().length;
     }

     postNotExists(post){
        let posts = this.getPosts();
        let filterPosts=posts.filter((p)=>p.postId===post.postId);
        return filterPosts.length>0 ? false : true;
    }

    getPostById(id) {
        let posts = this.getPosts();
        let filterPosts=posts.filter((p)=>p.postId===id);
        return filterPosts[0];
    }

    getTypedPosts()
    {   
        let result = new Array();
        let posts = this.getPosts();
        posts.forEach(element => {
            if(element.postText!== undefined){
                let textPost = new postText_PostText(element);
                result.push(textPost);
            }
            if(element.imgSrc!==undefined){
                let imagePost = new postImage_PostImage(element);
                result.push(imagePost);
            }
            if(element.videoSrc!==undefined){
                let videoPost = new postVideo_PostVideo(element);
                result.push(videoPost);
            }
        });
        return result;
    }

    getTypedPostsById(id){
        let posts = this.getTypedPosts();
        let filterPosts=posts.filter((p)=>p.postId===id);
        return filterPosts[0];
    }
}
// CONCATENATED MODULE: ./application/posts/postHelper/postHelper.js


let renderElementId;
const postFormRender = (wrapperElement) => {
    let textPostForm = document.createElement("form");
    textPostForm.id = "createPostForm";
    textPostForm.innerHTML = `
    <h3>Добавити новий пост:</h3>

        <input type="text" name='title' placeholder="Title" value="Title post"/>
        
        <select name="postType">
            <option value='text'>Text post</option>
            <option value='image'>Image post</option>
            <option value='video'>Video post</option>
        </select>

        <textarea name="testpost"><p>fjdhs jsdk  hfaskjdhkj sadhfk hsakjhfj afhsdk  hfaskjdhkj sadhfk hsakjhfj kshfsdk  hfaskjdhkj sadhfk hsakjhfj jkh fjsadf sjafhjash</p></textarea>
        <input type="hidden" name='imgSrc' placeholder="Image source" value="https://img-9gag-fun.9cache.com/photo/aA3K830_460swp.webp"/>
        <input type="hidden" name='videoSrc' placeholder="Video source" value="http://www.youtube.com/embed?listType=playlist&list=PLC77007E23FF423C6"/>

        <button type="submit">Добавити</button>
    `

    textPostForm.querySelector("select[name='postType']").addEventListener('change',postTypeChange(textPostForm));
    textPostForm.addEventListener('submit', savePost(textPostForm));

    wrapperElement.appendChild(textPostForm)
}

const postTypeChange = (textPostForm) =>{
    return function(e) {
        
        switch (textPostForm.postType.value) {
            case "video":
                textPostForm.videoSrc.setAttribute("type","text")
                textPostForm.imgSrc.setAttribute("type","hidden")
                textPostForm.testpost.setAttribute("hidden","hidden")
                break;
            case "image":
                textPostForm.imgSrc.setAttribute("type","text")
                textPostForm.testpost.setAttribute("hidden","hidden")
                textPostForm.videoSrc.setAttribute("type","hidden")
                break;
            default:
                textPostForm.testpost.removeAttribute("hidden")
                textPostForm.imgSrc.setAttribute("type","hidden")
                textPostForm.videoSrc.setAttribute("type","hidden")
                break;
        }
    }
}

const renderButtonAddNewPost = (newPostWrapperId, renderElement) => {
    renderElementId=renderElement;
    let addNewPostButton = document.createElement("button");
    addNewPostButton.classList.add("AddNewPost");
    addNewPostButton.innerText = "Добавити пост";
    addNewPostButton.addEventListener("click", ()=> {   
        togglePostForm(newPostWrapperId);
    });
    return addNewPostButton;
}

const togglePostForm = (newPostWrapperId) => {
    let wrapper = document.getElementById(newPostWrapperId);
    if(wrapper.querySelector("form")){
        wrapper.innerHTML = "";
    } else {
        postFormRender(wrapper);
    }
}

const savePost = (form) => {
    return function(e) {
        e.preventDefault();
        let postRepository = new postRepository_PostRepository();
        let userRepository = new userRepository_UserRepository();
        let post = {
            title: form.title.value,
            userId: userRepository.getCurrentUser().id,
            rate: [],
            publishDate: "just now"
        }
        
        switch (form.postType.value) {
            case "video":
                post["videoSrc"] = form.videoSrc.value;          
                break;
            case "image":
                post["imgSrc"] = form.imgSrc.value;          
                break;
            default:
                post["postText"] = form.testpost.value;          
                break;
        }
        console.log("Tut");
        let savedPost = postRepository.save(post); 
        let postForRender = postRepository.getTypedPostsById(savedPost.postId);
        console.log("postForRender",postForRender);
        let renderElement = document.getElementById(renderElementId);
        console.log("renderElement",renderElement);
        renderElement.appendChild(postForRender.render());
        console.log("renderElementId",renderElementId)
        console.log("renderElement", renderElement);
        console.log("postForRender",postForRender);
        //document.getElementById(renderElementId).appendChild(postForRender);
    }
}

// CONCATENATED MODULE: ./application/posts/index.js










// CONCATENATED MODULE: ./application/user/userHelper.js




const repository = new userRepository_UserRepository();
let userHelper_renderElementId;



const renderRegistration =() => {
    let userReistrationForm = document.createElement("form");
    userReistrationForm.id = "userReistrationForm";
    userReistrationForm.innerHTML = `
    <input type="text" name='name' placeholder="Ваше ім'я" value="Andrii"/>
    <input type="password" name='password1' placeholder="Пароль" value="321321" />
    <input type="password" name='password2' placeholder="Повторіть пароль" value="321321" />
    <input type="email" name="email" placeholder="Email" value="ostapchukandrii@gmail.com" />
    <input type="text" name="src" placeholder="Посилання на фото" value="https://pp.userapi.com/QKxHCl9M9_pCI8sEweYOhYzw5DlRGmWFs3cT9w/2J4Pih7JFZA.jpg" />
    <div id="errorReistrationForm"></div>
    <button type="button" name="submit" id="_submitRegistrationForm">Відправити</button>
    <button id="openLoginForm">Увійти</button>`;
    userReistrationForm.querySelector("#_submitRegistrationForm").addEventListener('click',checkRegistrationForm(userReistrationForm));
    userReistrationForm.querySelector("#openLoginForm").addEventListener('click', openLoginForm);
    return userReistrationForm;
}
//Check form 
const checkRegistrationForm =(form,ref)=>{
    return function(e){
        console.log("Форма для реєстрація:",form);
        const errorBox = form.querySelector("#errorReistrationForm");
        errorBox.innerHTML = '';
        let name=form.name.value;
        let password1 = form.password1.value;
        let password2 = form.password2.value;
        let email = form.email.value;
        let src = form.src.value;
        // To do періврки для форми
        
        if(name===""){
            errorBox.innerHTML="Ім'я не може бути пустим";
            return;
        }
        if(password1===""){
            errorBox.innerHTML="Пароль не може бути пустим";
            return;
        }
        if(password1!==password2){
            errorBox.innerHTML="Паролі не збігаються";
            return;
        }
        if(!checkEmail(email)){
            errorBox.innerHTML="Email заданий не вірно";
            return;
        }
        if(!checkImgLink(src)){
            errorBox.innerHTML="Посилання на фото задано не вірно";
            return;
        }
        // 
        let user = new UserModel(name,src,password1,email);
        let responce=repository.saveUser(user);
        if(responce.error===undefined)
        {
            repository.login(user.email, user.password);
            renderLoginElement(userHelper_renderElementId);
        }
        else {
            errorBox.innerHTML = responce.error;
        }
    }
}

const checkEmail =(str) => {
    var regExp = /^([\w\.\+]{1,})([^\W])(@)([\w]{1,})(\.[\w]{1,})+$/;
    console.log("Chcek email", regExp.test(str));
    return regExp.test(str);
}
const checkImgLink =(str) => {
    let myRegex = /(https?:\/\/.*\.(?:png|jpg))/i;
    return myRegex.test(str);
    
}


const renderLogin =() => {
    let userLoginForm = document.createElement("form");
    userLoginForm.id = "userLoginForm";
    userLoginForm.innerHTML = `
    <input type="email" name="email" placeholder="Email" value="ostapchukandrii@gmail.com" />
    <input type="password" name='password' placeholder="Пароль" value="321321" />
    <div id="errorLoginForm"></div>
    <button type="button" name="submit" id="_submitLoginForm">Увійти</button>
    <button id="openRegistrationForm">Реєстрація</button>`;
    userLoginForm.querySelector("#_submitLoginForm").addEventListener('click',checkLoginForm(userLoginForm));
    userLoginForm.querySelector("#openRegistrationForm").addEventListener('click', openRegistrationForm);
    return userLoginForm;
}

const checkLoginForm =(form,ref)=>{
    return function(e){
        console.log("Форма для логіну:",form);
        const errorBox = form.querySelector("#errorLoginForm");
        let email = form.email.value;
        let password = form.password.value;
        //To Do перевірки для форми

        let responce=repository.login(email,password);
        if(responce.error===undefined)
        {
            console.log(responce);
            renderLoginElement(userHelper_renderElementId);
        }
        else {
            errorBox.innerHTML = responce.error;
        }
    }
}

const renderLoginElement =(renderId) => {
    userHelper_renderElementId = renderId;
    let renderElement = document.getElementById(userHelper_renderElementId);
    renderElement.innerHTML="";
    let user = repository.getCurrentUser();
    console.log(repository.getCurrentUser());
    if(repository.getCurrentUser()!==null)
    {
        let loginElement = user.render();
        let logOutButton = document.createElement("button");
        logOutButton.type="button";
        logOutButton.innerHTML="Вийти";
        logOutButton.addEventListener("click", ()=> {
            repository.removeCurrentUser();
            renderLoginElement(userHelper_renderElementId);
        });
        loginElement.querySelector(".userInfo").appendChild(logOutButton);
        renderElement.appendChild(loginElement);
    }
    else {
        renderElement.appendChild(renderLogin());
    }   
}

const openRegistrationForm =() => {
    let renderElement = document.getElementById(userHelper_renderElementId);
    renderElement.innerHTML="";
    renderElement.appendChild(renderRegistration());
}

const openLoginForm =() => {
    let renderElement = document.getElementById(userHelper_renderElementId);
    renderElement.innerHTML="";
    renderElement.appendChild(renderLogin());
}


/* harmony default export */ var userHelper = ({renderLoginElement});
// CONCATENATED MODULE: ./application/user/index.js



/* harmony default export */ var application_user = (userHelper);


// CONCATENATED MODULE: ./application/index.js





document.addEventListener('DOMContentLoaded', ()=>{

  let postRepository = new postRepository_PostRepository();
  let posts=postRepository.getTypedPosts();

  console.log("POSTS:",posts)
  // Existing posts
  posts.forEach(element => {
    document.getElementById("posts").appendChild(element.render());  
  });
  
  // Registration
  application_user.renderLoginElement("registration");
  document.getElementById("leftPanel").appendChild(renderButtonAddNewPost("addNewPostWrapper", "posts"));  
 
});



/***/ }),
/* 1 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);