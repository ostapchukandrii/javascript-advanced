import {UserRepository} from "../../user/userRepository"

export class PostBase {

    constructor({title, userId, rate, postId}) {
        this.postId = postId;
        this.title = title;
        this.userId = userId;
        this.rate = rate;        
    }

    render(addtionalClass, content) {

        let userRepository = new UserRepository();
        let user = userRepository.getUserById(this.userId);
 
        let authorImg = user.imgSrc;
        let authorName = user.name;
        let publishDate = "4h";
        
        let baseElement = document.createElement("div");
        baseElement.classList.add("post");

        if(addtionalClass){
            baseElement.classList.add(addtionalClass);
        }
        
        //add header
        let postHeader = document.createElement("div");
        postHeader.classList.add("post-header");        
        postHeader.innerHTML = `<img class="avatar" src="${authorImg}" alt="Author">
                                <p class="author">${authorName}</p> <p>|</p> <p class="publishDate">${publishDate}</p>`;
        baseElement.appendChild(postHeader);
        
        //add title
        let postTitle = document.createElement("div");
        postTitle.classList.add("post-title");
        postTitle.innerText = this.title;
        baseElement.appendChild(postTitle);

        //add content
        let postContent = document.createElement("div");
        postContent.classList.add("post-content");
        if(content){
            postContent.innerHTML = content.innerHTML;
        } else {
            postContent.innerText = "CONTENT";
        }
        baseElement.appendChild(postContent);

        //add post info
        let rate = document.createElement("div");
        rate.classList.add("post-info");
        rate.innerHTML = `<button class="up">UP</button>
                          <p class="rate">${this.rate.length}</p>
                          <button class="down">DOWN</button>
                          `
        baseElement.appendChild(rate);

        baseElement.querySelector(".up").addEventListener("click", upRate(baseElement));
        baseElement.querySelector(".down").addEventListener("click", downRate(baseElement));
        
        return baseElement; 
    }
} 

const upRate = (post) => {
    return function(e){
        post.querySelector(".rate").innerText = +post.querySelector(".rate").innerText + 1;
    }
}

const downRate = (post) => {
    return function(e){
        post.querySelector(".rate").innerText = +post.querySelector(".rate").innerText - 1;
    }
}
