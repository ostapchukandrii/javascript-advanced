import {PostBase} from "../basePost/postBase"

export class PostText extends PostBase{
    constructor({title, userId, rate, postText, postId=0}){
        super({title, userId, rate, postId});
        this.postText = postText;
    }

    render(){
        let content = document.createElement("div");
        content.innerHTML = this.postText;
        return super.render("post-text",content);
    }
}