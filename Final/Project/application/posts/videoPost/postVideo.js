import {PostBase} from "../basePost/postBase"

export class PostVideo extends PostBase{
    constructor({title, userId, rate, videoSrc, postId=0}){
        super({title, userId, rate, postId});
        this.videoSrc = videoSrc;
    }

    render(){
        let content = document.createElement("div");
        content.innerHTML = `<iframe width="650" height="488"
                                src="${this.videoSrc}">
                            </iframe>`;
        return super.render("post-video",content);
    }
}