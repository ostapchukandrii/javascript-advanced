import {PostText} from "../textPost/postText";
import {PostImage} from "../imagePost/postImage";
import {PostVideo} from "../videoPost/postVideo";

export class PostRepository {
    constructor(){
          // Posts

          let postImage = new PostImage({postId:1, title: "Title test", userId: 1, rate: [1], imgSrc: "https://img-9gag-fun.9cache.com/photo/aA3K830_460swp.webp"});
          let postImage2 = new PostImage({postId:2, title: "Title test", userId: 0, rate: [0], imgSrc: "https://img-9gag-fun.9cache.com/photo/aA3K830_460swp.webp"});

          let postTextModel = {postId: 3, title: "Title test", userId: 1, rate: [1],
          postText: `<p>fjdhs jafhsdk  hfaskjdhkj sadhfk hsakjhfj kshfjkh fjsadf sjafhjash</p>
          <p>fjdhs jsdk  hfaskjdhkj sadhfk hsakjhfj afhsdk  hfaskjdhkj sadhfk hsakjhfj kshfsdk  hfaskjdhkj sadhfk hsakjhfj jkh fjsadf sjafhjash</p>
          <p>fjdhs jasdk  hfaskjdhkj sadhfk hsakjhfj fhsdk  hfaskjdhkj sadhfk hsakjhfj kshfjkh fjsadf sjafhjash</p>
          <p>fjdhs jafhsdk  hfaskjdhkj sadhfk hsakjhfsdk  hfaskjdhkj sadhfk hsakjhfj sdk  hfaskjdhkj sadhfk hsakjhfj sdk  hfaskjdhkj sadhfk hsakjhfj j kshfjkh fjsadf sjafhjash</p>
          <p>fjdhs jafhsdk  hfaskjdhkj sadhfk hsakjhfj kssdk  hfaskjdhkj sadhfk hsakjhfj sdk  hfaskjdhkj sadhfk hsakjhfj hfjkh fjsadf sjafhjash</p>
          <p>fjdhs jafhsdk  hfaskjdhkj sadhfk hsakjhfj kshfjkh fjsadf sjafhjash</p>
          <p>fjdhs jafhsdk  hfaskjdhkj sadhfk hsakjhsdk  hfaskjdhkj sadhfk hsakjhfj sdk  hfaskjdhkj sadhfk hsakjhfj sdk  hfaskjdhkj sadhfk hsakjhfj fsdk  hfaskjdhkj sadhfk hsakjhfj sdk  hfaskjdhkj sadhfk hsakjhfj vj kshfjkh fjsadf sjafhjash</p>
          <p>fjdhs jafhsdk  hfaskjdhkj sadhfk hsakjhfj sdk  hfaskjdhkj sadhfk sdk  hfaskjdhkj sadhfk hsakjhfj vhsasdk  hfaskjdhkj sadhfk hsakjhfj sdk  hfaskjdhkj sadhfk hsakjhfj vkjhfj kshfjkh fjsadf sjafhjash</p>
          `};

           let postText = new PostText(postTextModel);
           let postVideo = new PostVideo({postId: 4, title: "Title test",userId: 1, rate: [1], videoSrc: "https://www.youtube.com/embed/tgbNymZ7vqY"});
           
           this.save(postText);
           this.save(postImage);
           this.save(postVideo);
           this.save(postImage2);
    }

    save(post){
        console.log("postForSave",post);
        if(post.postId===0 || post.postId===undefined){
            post.postId=this.getPostId();
        }


        if(this.postNotExists(post))
        {
            let posts = this.getPosts();
            posts.push(post);
            localStorage.setItem("posts",JSON.stringify(posts));
        }
        else
        {
            return {error: "Помилка! Пост з таким ідентифікатором уже існує."};
        }
        console.log("newPost",post);
        return post;
    }

    getPosts(){
        let posts = JSON.parse(localStorage.getItem("posts"));
        if(posts === null)
        {
            posts = new Array();
        }
        return posts;
    }

    getPostId(){
        return this.getPosts().length;
     }

     postNotExists(post){
        let posts = this.getPosts();
        let filterPosts=posts.filter((p)=>p.postId===post.postId);
        return filterPosts.length>0 ? false : true;
    }

    getPostById(id) {
        let posts = this.getPosts();
        let filterPosts=posts.filter((p)=>p.postId===id);
        return filterPosts[0];
    }

    getTypedPosts()
    {   
        let result = new Array();
        let posts = this.getPosts();
        posts.forEach(element => {
            if(element.postText!== undefined){
                let textPost = new PostText(element);
                result.push(textPost);
            }
            if(element.imgSrc!==undefined){
                let imagePost = new PostImage(element);
                result.push(imagePost);
            }
            if(element.videoSrc!==undefined){
                let videoPost = new PostVideo(element);
                result.push(videoPost);
            }
        });
        return result;
    }

    getTypedPostsById(id){
        let posts = this.getTypedPosts();
        let filterPosts=posts.filter((p)=>p.postId===id);
        return filterPosts[0];
    }
}