import {PostRepository} from "../postRepository/postRepository"
import {UserRepository} from "../../user/userRepository"
let renderElementId;
export const postFormRender = (wrapperElement) => {
    let textPostForm = document.createElement("form");
    textPostForm.id = "createPostForm";
    textPostForm.innerHTML = `
    <h3>Добавити новий пост:</h3>

        <input type="text" name='title' placeholder="Title" value="Title post"/>
        
        <select name="postType">
            <option value='text'>Text post</option>
            <option value='image'>Image post</option>
            <option value='video'>Video post</option>
        </select>

        <textarea name="testpost"><p>fjdhs jsdk  hfaskjdhkj sadhfk hsakjhfj afhsdk  hfaskjdhkj sadhfk hsakjhfj kshfsdk  hfaskjdhkj sadhfk hsakjhfj jkh fjsadf sjafhjash</p></textarea>
        <input type="hidden" name='imgSrc' placeholder="Image source" value="https://img-9gag-fun.9cache.com/photo/aA3K830_460swp.webp"/>
        <input type="hidden" name='videoSrc' placeholder="Video source" value="http://www.youtube.com/embed?listType=playlist&list=PLC77007E23FF423C6"/>

        <button type="submit">Добавити</button>
    `

    textPostForm.querySelector("select[name='postType']").addEventListener('change',postTypeChange(textPostForm));
    textPostForm.addEventListener('submit', savePost(textPostForm));

    wrapperElement.appendChild(textPostForm)
}

const postTypeChange = (textPostForm) =>{
    return function(e) {
        
        switch (textPostForm.postType.value) {
            case "video":
                textPostForm.videoSrc.setAttribute("type","text")
                textPostForm.imgSrc.setAttribute("type","hidden")
                textPostForm.testpost.setAttribute("hidden","hidden")
                break;
            case "image":
                textPostForm.imgSrc.setAttribute("type","text")
                textPostForm.testpost.setAttribute("hidden","hidden")
                textPostForm.videoSrc.setAttribute("type","hidden")
                break;
            default:
                textPostForm.testpost.removeAttribute("hidden")
                textPostForm.imgSrc.setAttribute("type","hidden")
                textPostForm.videoSrc.setAttribute("type","hidden")
                break;
        }
    }
}

export const renderButtonAddNewPost = (newPostWrapperId, renderElement) => {
    renderElementId=renderElement;
    let addNewPostButton = document.createElement("button");
    addNewPostButton.classList.add("AddNewPost");
    addNewPostButton.innerText = "Добавити пост";
    addNewPostButton.addEventListener("click", ()=> {   
        togglePostForm(newPostWrapperId);
    });
    return addNewPostButton;
}

const togglePostForm = (newPostWrapperId) => {
    let wrapper = document.getElementById(newPostWrapperId);
    if(wrapper.querySelector("form")){
        wrapper.innerHTML = "";
    } else {
        postFormRender(wrapper);
    }
}

const savePost = (form) => {
    return function(e) {
        e.preventDefault();
        let postRepository = new PostRepository();
        let userRepository = new UserRepository();
        let post = {
            title: form.title.value,
            userId: userRepository.getCurrentUser().id,
            rate: [],
            publishDate: "just now"
        }
        
        switch (form.postType.value) {
            case "video":
                post["videoSrc"] = form.videoSrc.value;          
                break;
            case "image":
                post["imgSrc"] = form.imgSrc.value;          
                break;
            default:
                post["postText"] = form.testpost.value;          
                break;
        }
        console.log("Tut");
        let savedPost = postRepository.save(post); 
        let postForRender = postRepository.getTypedPostsById(savedPost.postId);
        console.log("postForRender",postForRender);
        let renderElement = document.getElementById(renderElementId);
        console.log("renderElement",renderElement);
        renderElement.appendChild(postForRender.render());
        console.log("renderElementId",renderElementId)
        console.log("renderElement", renderElement);
        console.log("postForRender",postForRender);
        //document.getElementById(renderElementId).appendChild(postForRender);
    }
}
