import {PostBase} from "../basePost/postBase"

export class PostImage extends PostBase{
    constructor({title, userId, rate, imgSrc, postId=0}){
        super({title, userId, rate, postId});
        this.imgSrc = imgSrc;
    }

    render(){
        let content = document.createElement("div");
        content.innerHTML = `<img class="avatar" src="${this.imgSrc}" alt="Author">`;
        return super.render("post-img",content);
    }
}