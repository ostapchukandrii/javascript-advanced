import "./style.scss"

import {PostImage, PostText, PostVideo, PostRepository, renderButtonAddNewPost} from './posts/index';
import userFunction from './user';

document.addEventListener('DOMContentLoaded', ()=>{

  let postRepository = new PostRepository();
  let posts=postRepository.getTypedPosts();

  console.log("POSTS:",posts)
  // Existing posts
  posts.forEach(element => {
    document.getElementById("posts").appendChild(element.render());  
  });
  
  // Registration
  userFunction.renderLoginElement("registration");
  document.getElementById("leftPanel").appendChild(renderButtonAddNewPost("addNewPostWrapper", "posts"));  
 
});

