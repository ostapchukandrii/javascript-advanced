export class UserModel {
    constructor(name, imgSrc, password, email, id=0){
        this.id = id;
        this.name = name;
        this.imgSrc = imgSrc;
        this.email = email;
        this.password = password;
    }

    render() {
        let userBlock = document.createElement("div");
        userBlock.classList.add("userBlock");
        userBlock.innerHTML = `<div><img src=${this.imgSrc} alt="userLogo"></div>
        <div class="userInfo"><h4>${this.name}</h4><p>Email: ${this.email}</p></div>`;
        return userBlock;
    }
}