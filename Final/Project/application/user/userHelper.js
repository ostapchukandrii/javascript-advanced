import { UserModel } from "./userModel";
import { UserRepository } from './userRepository';
import { postFormRender } from "../posts/index"

const repository = new UserRepository();
let renderElementId;



const renderRegistration =() => {
    let userReistrationForm = document.createElement("form");
    userReistrationForm.id = "userReistrationForm";
    userReistrationForm.innerHTML = `
    <input type="text" name='name' placeholder="Ваше ім'я" value="Andrii"/>
    <input type="password" name='password1' placeholder="Пароль" value="321321" />
    <input type="password" name='password2' placeholder="Повторіть пароль" value="321321" />
    <input type="email" name="email" placeholder="Email" value="ostapchukandrii@gmail.com" />
    <input type="text" name="src" placeholder="Посилання на фото" value="https://pp.userapi.com/QKxHCl9M9_pCI8sEweYOhYzw5DlRGmWFs3cT9w/2J4Pih7JFZA.jpg" />
    <div id="errorReistrationForm"></div>
    <button type="button" name="submit" id="_submitRegistrationForm">Відправити</button>
    <button id="openLoginForm">Увійти</button>`;
    userReistrationForm.querySelector("#_submitRegistrationForm").addEventListener('click',checkRegistrationForm(userReistrationForm));
    userReistrationForm.querySelector("#openLoginForm").addEventListener('click', openLoginForm);
    return userReistrationForm;
}
//Check form 
const checkRegistrationForm =(form,ref)=>{
    return function(e){
        console.log("Форма для реєстрація:",form);
        const errorBox = form.querySelector("#errorReistrationForm");
        errorBox.innerHTML = '';
        let name=form.name.value;
        let password1 = form.password1.value;
        let password2 = form.password2.value;
        let email = form.email.value;
        let src = form.src.value;
        // To do періврки для форми
        
        if(name===""){
            errorBox.innerHTML="Ім'я не може бути пустим";
            return;
        }
        if(password1===""){
            errorBox.innerHTML="Пароль не може бути пустим";
            return;
        }
        if(password1!==password2){
            errorBox.innerHTML="Паролі не збігаються";
            return;
        }
        if(!checkEmail(email)){
            errorBox.innerHTML="Email заданий не вірно";
            return;
        }
        if(!checkImgLink(src)){
            errorBox.innerHTML="Посилання на фото задано не вірно";
            return;
        }
        // 
        let user = new UserModel(name,src,password1,email);
        let responce=repository.saveUser(user);
        if(responce.error===undefined)
        {
            repository.login(user.email, user.password);
            renderLoginElement(renderElementId);
        }
        else {
            errorBox.innerHTML = responce.error;
        }
    }
}

const checkEmail =(str) => {
    var regExp = /^([\w\.\+]{1,})([^\W])(@)([\w]{1,})(\.[\w]{1,})+$/;
    console.log("Chcek email", regExp.test(str));
    return regExp.test(str);
}
const checkImgLink =(str) => {
    let myRegex = /(https?:\/\/.*\.(?:png|jpg))/i;
    return myRegex.test(str);
    
}


const renderLogin =() => {
    let userLoginForm = document.createElement("form");
    userLoginForm.id = "userLoginForm";
    userLoginForm.innerHTML = `
    <input type="email" name="email" placeholder="Email" value="ostapchukandrii@gmail.com" />
    <input type="password" name='password' placeholder="Пароль" value="321321" />
    <div id="errorLoginForm"></div>
    <button type="button" name="submit" id="_submitLoginForm">Увійти</button>
    <button id="openRegistrationForm">Реєстрація</button>`;
    userLoginForm.querySelector("#_submitLoginForm").addEventListener('click',checkLoginForm(userLoginForm));
    userLoginForm.querySelector("#openRegistrationForm").addEventListener('click', openRegistrationForm);
    return userLoginForm;
}

const checkLoginForm =(form,ref)=>{
    return function(e){
        console.log("Форма для логіну:",form);
        const errorBox = form.querySelector("#errorLoginForm");
        let email = form.email.value;
        let password = form.password.value;
        //To Do перевірки для форми

        let responce=repository.login(email,password);
        if(responce.error===undefined)
        {
            console.log(responce);
            renderLoginElement(renderElementId);
        }
        else {
            errorBox.innerHTML = responce.error;
        }
    }
}

const renderLoginElement =(renderId) => {
    renderElementId = renderId;
    let renderElement = document.getElementById(renderElementId);
    renderElement.innerHTML="";
    let user = repository.getCurrentUser();
    console.log(repository.getCurrentUser());
    if(repository.getCurrentUser()!==null)
    {
        let loginElement = user.render();
        let logOutButton = document.createElement("button");
        logOutButton.type="button";
        logOutButton.innerHTML="Вийти";
        logOutButton.addEventListener("click", ()=> {
            repository.removeCurrentUser();
            renderLoginElement(renderElementId);
        });
        loginElement.querySelector(".userInfo").appendChild(logOutButton);
        renderElement.appendChild(loginElement);
    }
    else {
        renderElement.appendChild(renderLogin());
    }   
}

const openRegistrationForm =() => {
    let renderElement = document.getElementById(renderElementId);
    renderElement.innerHTML="";
    renderElement.appendChild(renderRegistration());
}

const openLoginForm =() => {
    let renderElement = document.getElementById(renderElementId);
    renderElement.innerHTML="";
    renderElement.appendChild(renderLogin());
}


export default {renderLoginElement};