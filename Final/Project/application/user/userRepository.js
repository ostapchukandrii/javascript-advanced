import { UserModel } from "./userModel";

export class UserRepository {
    constructor(){
        this.saveUser(new UserModel("Andrii", "https://i.pinimg.com/236x/58/83/46/5883460be4596cb67bd791acaeb32888.jpg", "321321", "ostapchukandrii@gmail.com", this.getUserId()));
        this.saveUser(new UserModel("Nazar", "https://pp.userapi.com/QKxHCl9M9_pCI8sEweYOhYzw5DlRGmWFs3cT9w/2J4Pih7JFZA.jpg", "321321", "nazarmyua@gmail.com",this.getUserId()));
    }

    saveUser(user){
        if(user.id===0){
            user.id=this.getUserId();
        }
        if(this.userNotExists(user))
        {
            let users = this.getUsers();
            users.push(user);
            localStorage.setItem("users",JSON.stringify(users));
        }
        else
        {
            return {error: "Помилка! Такий користувач уже існує."};
        }
        return user;
    }

    userNotExists(user){
        let users = this.getUsers();
        let filterUsers=users.filter((u)=>u.email===user.email);
        return filterUsers.length>0? false : true;
    }

    getUsers(){
        let users = JSON.parse(localStorage.getItem("users"));
        if(users === null)
        {
            users = new Array();
        }
        return users;
    }

    getUserById(id) {
        let users = this.getUsers();
        let filterUsers=users.filter((u)=>u.id===id);
        return filterUsers[0];
    }

    getUserId(){
       return this.getUsers().length;
    }

    login(email,password){
        let users = this.getUsers();
        let filterUsers=users.filter((u)=>u.email===email && u.password===password);
        if(filterUsers.length===0){
            return {error: "Помилка! Неправильний логін або пароль."};
        }
        let currentUser = filterUsers[0];
        this.setCurrentUser(currentUser);
        return currentUser;
    }

    // currentUser
    getCurrentUser (){
        let u = JSON.parse(localStorage.getItem("user"));
        if(u===null) 
        return u;
        return new UserModel(u.name, u.imgSrc, u.password, u.email, u.id);
    }

    removeCurrentUser(){
        localStorage.removeItem("user");
    }

    setCurrentUser(user){
        this.removeCurrentUser();
        localStorage.setItem("user",JSON.stringify(user));
    }

}